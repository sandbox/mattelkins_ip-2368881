/**
 * @license Copyright (c) CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.plugins.add('wordcount', {
    lang: 'ca,de,el,en,es,fr,it,jp,nl,no,pl,pt-br,ru,sv',
    version: 1.10,
    init: function(editor) {
        if (editor.elementMode === CKEDITOR.ELEMENT_MODE_INLINE) {
            return;
        }

        var defaultFormat = '<span class="cke_path_item">',
            intervalId,
            lastWordCount,
            lastCharCount = 0;

        var defaultConfig = {
            showWordCount: true,
            showCharCount: false,
            countSpacesAsChars: false,
            countHTML: false
        };

        var config = CKEDITOR.tools.extend(defaultConfig, editor.config.wordcount || {}, true);

        if (config.showCharCount) {
            var charLabel = editor.lang.wordcount[config.countHTML ? 'CharCountWithHTML' : 'CharCount'];

            defaultFormat += charLabel + '&nbsp;%charCount%';
        }

        if (config.showCharCount && config.showWordCount) {
            defaultFormat += ',&nbsp;';
        }

        if (config.showWordCount) {
            defaultFormat += editor.lang.wordcount.WordCount + ' %wordCount%';
        }

        defaultFormat += '</span>';

        var format = defaultFormat;

        CKEDITOR.document.appendStyleSheet(this.path + 'css/wordcount.css');

        function counterId(editorInstance) {
            return 'cke_wordcount_' + editorInstance.name;
        }

        function counterElement(editorInstance) {
            return document.getElementById(counterId(editorInstance));
        }

        function strip(html) {
            var tmp = document.createElement("div");
            tmp.innerHTML = html;

            if (tmp.textContent == '' && typeof tmp.innerText == 'undefined') {
                return '';
            }

            return tmp.textContent || tmp.innerText;
        }

        function updateCounter(editorInstance) {
            var wordCount = 0,
                charCount = 0,
                normalizedText,
                text;

            if (text = editorInstance.getData()) {
                if (config.showCharCount) {
                    if (config.countHTML) {
                        charCount = text.length;
                    } else {
                        if (editor.config.fullPage) {
                            var i = text.search(new RegExp("<body>", "i"));
                            if (i != -1) {
                                var j = text.search(new RegExp("</body>", "i"));
                                text = text.substring(i + 6, j);
                            }

                        }

                        normalizedText = text.
                            replace(/(\r\n|\n|\r)/gm, "").
                            replace(/^\s+|\s+$/g, "").
                            replace("&nbsp;", "");

                        if (!config.countSpacesAsChars) {
                            normalizedText = text.
                                replace(/\s/g, "");
                        }

                        normalizedText = strip(normalizedText).replace(/^([\s\t\r\n]*)$/, "");

                        charCount = normalizedText.length;
                    }
                }

                if (config.showWordCount) {
                    normalizedText = text.
                        replace(/(\r\n|\n|\r)/gm, " ").
                        replace(/^\s+|\s+$/g, "").
                        replace("&nbsp;", " ");

                    normalizedText = strip(normalizedText);

                    var words = normalizedText.split(/\s+/);

                    for (var wordIndex = words.length - 1; wordIndex >= 0; wordIndex--) {
                        if (words[wordIndex].match(/^([\s\t\r\n]*)$/)) {
                            words.splice(wordIndex, 1);
                        }
                    }

                    wordCount = words.length;
                }
            }

            var html = format.replace('%wordCount%', wordCount).replace('%charCount%', charCount);

            editor.plugins.wordcount.wordCount = wordCount;
            editor.plugins.wordcount.charCount = charCount;

            counterElement(editorInstance).innerHTML = html;

            if (charCount == lastCharCount) {
                return true;
            }

            lastWordCount = wordCount;
            lastCharCount = charCount;

            return true;
        }

        editor.on('key', function(event) {
            if (editor.mode === 'source') {
                updateCounter(event.editor);
            }
        }, editor, null, 100);

        editor.on('change', function(event) {

            updateCounter(event.editor);
        }, editor, null, 100);

        editor.on('uiSpace', function(event) {
            if (event.data.space == 'bottom') {
                event.data.html += '<div id="' + counterId(event.editor) + '" class="cke_wordcount" style=""' + ' title="' + editor.lang.wordcount.title + '"' + '>&nbsp;</div>';
            }
        }, editor, null, 100);

        editor.on('dataReady', function(event) {
            updateCounter(event.editor);
        }, editor, null, 100);

        editor.on('afterPaste', function(event) {
            updateCounter(event.editor);
        }, editor, null, 100);

        editor.on('blur', function() {
            if (intervalId) {
                window.clearInterval(intervalId);
            }
        }, editor, null, 300);

        if (!String.prototype.trim) {
            String.prototype.trim = function() {
                return this.replace(/^\s+|\s+$/g, '');
            };
        }
    }
});
